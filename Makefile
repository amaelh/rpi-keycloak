.PHONY: compile build test sast sca container-scanning iac-sast

##################################################################################################
# Top level targets
##################################################################################################
default: test sast sca container-scanning iac-sast

##################################################################################################
# Variables
##################################################################################################
# Dossier projet
PROJECT_DIR := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
PROJECT_NAME := $(notdir $(patsubst %/,%,$(PROJECT_DIR)))
PROJECT_GROUP := amaelh
PROJECT_MAINTAINER := amael@heiligtag.com

# Image de base
BASE_IMAGE := $(shell find $(PROJECT_DIR) -name Dockerfile | xargs grep FROM | tail -1 | awk '{print $$2}')

# Image a construire
CI_REGISTRY_IMAGE ?= registry.gitlab.com/$(PROJECT_GROUP)/$(PROJECT_NAME)

# Tag
## On esaye de recuperer le nom de la branche courante depuis git
GIT_BRANCH := $(shell /usr/bin/git symbolic-ref HEAD | sed 's!refs\/heads\/!!')
# Par défaut on laisse git nous donner le tag, sinon on essaye d'utiliser le nom de la branche git
# Et si on n'a pas trouvé la branche, on utilise "development"
CI_COMMIT_REF_NAME ?= $(or $(GIT_BRANCH),development)
# Version précise pour les labels
CI_COMMIT_SHA ?= $(shell git describe --tags --always --abbrev=40 --dirty)
BUILD_DATE := $(shell date -u +'%Y-%m-%dT%H:%M:%SZ')

# On récupère la partie X.Y du nommage semver X.Y.Z-metadata, le cas échéant
SEMVER := $(shell echo ${CI_COMMIT_REF_NAME} | sed 's/\([0-9]\+.[0-9]\+\).[0-9]\+.*$$/\1/')
MAJORMINOR_VERSION := $(if $(strip $(SEMVER)),$(SEMVER),$(CI_COMMIT_REF_NAME))

# Architecture
BUILD_ARCH := linux-$(shell dpkg --print-architecture | awk -F'-' '{print $$NF}')
RELEASE_ARCH := linux/arm/v6,linux/arm/v7,linux/arm64/v8,linux/amd64

# Type de projet
IS_NODEJS := $(shell test 0 -ne $(shell find $(PROJECT_DIR) -name "node_modules" -prune -o -name package.json | wc -l) && echo "yes")
IS_GRADLE := $(shell test 0 -ne $(shell find $(PROJECT_DIR) -name build.gradle | wc -l) && echo "yes")
IS_DOCKER := $(shell test 0 -ne $(shell find $(PROJECT_DIR) -name Dockerfile | wc -l) && echo "yes")
IS_PYTHON := $(shell test 0 -ne $(shell find $(PROJECT_DIR) -name requirements.txt | wc -l) && echo "yes")

# Licences applicables : par défaut MIT
LICENCES ?= MIT

# Include project-specific Makefiles if any exists 
# Note : include these files after setting variables to be able to replace their values
-include Makefile.*.mk

# Compute artefact name after including other makefiles, to be able to replace some of the base variables
DOCKER_IMAGE_TAGNAME := $(CI_REGISTRY_IMAGE):$(CI_COMMIT_REF_NAME)
DOCKER_IMAGE_TAGMAJORMINOR := $(CI_REGISTRY_IMAGE):$(MAJORMINOR_VERSION)
DOCKER_IMAGE_TAGLATEST := $(CI_REGISTRY_IMAGE):latest

##################################################################################################
# Single architecture builds for testing purposes
##################################################################################################
compile: 
	$(warning ###################################################################)
	$(warning Compiling application without building final docker image)
	docker build --pull --target builder -t $(DOCKER_IMAGE_TAGNAME)-$(BUILD_ARCH)-builder .

build:
	$(warning ###################################################################)
	$(warning Building single architecture image)
	docker build --pull \
		--label "org.opencontainers.image.authors=$(PROJECT_MAINTAINER)" \
		--label "org.opencontainers.image.created=$(BUILD_DATE)" \
		--label "org.opencontainers.image.base.name=$(BASE_IMAGE)" \
		--label "org.opencontainers.image.version=$(CI_COMMIT_REF_NAME)" \
		--label "org.opencontainers.image.revision=$(CI_COMMIT_SHA)" \
		--label "org.opencontainers.image.source=https://gitlab.com/$(PROJECT_GROUP)/$(PROJECT_NAME)" \
		--label "org.opencontainers.image.licenses=$(LICENCES)" \
		--label "built-for=$(BUILD_ARCH)" \
		-t $(DOCKER_IMAGE_TAGNAME)-$(BUILD_ARCH) .

test: test-os

test-os: build
	$(warning ###################################################################)
	$(warning Testing single architecture image image : OS)
	docker inspect --format='{{index .Config.Labels "org.opencontainers.image.base.name" }}' $(DOCKER_IMAGE_TAGNAME)-$(BUILD_ARCH) | grep distroless >/dev/null \
		&& echo "\n=> No OS testing for distroless-derivated images, skipping.\n" \
		|| docker run --rm \
				--entrypoint "" \
				$(DOCKER_IMAGE_TAGNAME)-$(BUILD_ARCH) \
				sh -c 'uname -a && echo && cat /etc/*-release && echo && /bin/echo "=> OS OK" && echo ""'

##################################################################################################
# SAST
##################################################################################################
sast: trivy-sast nodejsscan-sast snyk-code-sast

# Trivy
# ------------------------------------------------------------------------------------------------
# Default output for trivy sast
TRIVY_SAST_OUTPUT_ARG := --format template --template "@contrib/sarif.tpl" -o /project/reports/sast-trivy-$(PROJECT_NAME).sarif

trivy-sast: init-reports
	$(warning ###################################################################)
	$(warning SAST security testing with trivy)
	docker run --pull always --rm \
			-v $${HOME}/.cache/trivy:/root/.cache/trivy:rw \
			-v $(PROJECT_DIR):/project/:rw \
			aquasec/trivy:latest \
			fs --security-checks vuln,config --no-progress $(TRIVY_SAST_OUTPUT_ARG) /project/

# Alternate output : as table on stdout
trivy-sast-stdout: trivy-sast
trivy-sast-stdout: TRIVY_SAST_OUTPUT_ARG = --format table
# Alternate output : as gitlab-ci report
trivy-sast-gitlab: trivy-sast
trivy-sast-gitlab: TRIVY_SAST_OUTPUT_ARG = --format template --template "@contrib/gitlab.tpl" -o /project/reports/gl-sast-trivy-$(PROJECT_NAME).json 

# NodeJS Scan
# ------------------------------------------------------------------------------------------------
# Default output for node-js-scan
NODEJSSCAN_OUTPUT_ARG =	--sarif -o /project/reports/sast-nodejsscan-$(PROJECT_NAME).sarif

ifeq ($(IS_NODEJS),yes)
nodejsscan-sast: init-reports
	$(warning ###################################################################)
	$(warning SAST security testing with node-js-scan)
	docker run --pull always --rm \
			-v $(PROJECT_DIR):/project/:rw \
			opensecurity/njsscan \
			$(NODEJSSCAN_OUTPUT_ARG) /project/
else 
# No need to run NodeJS scan if this is not a NodeJS project :)
nodejsscan-sast:
endif

# Alternate output : as gitlab-ci report
nodejsscan-sast-gitlab: nodejsscan-sast
nodejsscan-sast-gitlab: NODEJSSCAN_OUTPUT_ARG =	--json -o /project/reports/gl-sast-nodejsscan-$(PROJECT_NAME).json


# Snyk code Scan
# ------------------------------------------------------------------------------------------------
# Default output for snyk-code-scan
SNYK_CODE_OUTPUT_ARG =	--sarif test' > $(PROJECT_DIR)/reports/sast-snyk-code-$(PROJECT_NAME).sarif

snyk-code-sast: init-reports snyk-login
	$(warning ###################################################################)
	$(warning SAST security testing with snyk code)
	docker run --pull always --rm \
			-v ~/.config/configstore/snyk.json:/root/.config/configstore/snyk.json:rw \
			-v $(PROJECT_DIR)/reports:/output/:rw \
			-v $(PROJECT_DIR):/project/:rw \
			-e SNYK_TOKEN \
			--entrypoint "" \
			snyk/snyk-cli:docker \
			sh -c 'cd /project && snyk code $(SNYK_CODE_OUTPUT_ARG) || true

# Alternate output : as json
snyk-code-sast-json: snyk-code-sast
snyk-code-sast-json: SNYK_CODE_OUTPUT_ARG =	--json test' > $(PROJECT_DIR)/reports/sast-snyk-code-$(PROJECT_NAME).json

# Alternate output : as html
snyk-code-sast-html: snyk-code-sast
snyk-code-sast-html: SNYK_CODE_OUTPUT_ARG =	--json test | snyk-to-html -o /output/sast-snyk-code-$(PROJECT_NAME).html'

##################################################################################################
# SCA : Software composition / Open source / Dependencies analysis
##################################################################################################
sca: dependency-check snyk-open-source

# OWASP Dependency Check
# ------------------------------------------------------------------------------------------------
# Default output for dependency check
DEPENDENCY_CHECK_OUTPUT_ARG := --format "SARIF" --out /project/reports/sca-dependency-check-$(PROJECT_NAME).sarif

dependency-check: update-dependencies-from-builder init-reports
	$(warning ###################################################################)
	$(warning Software composition analysis with dependency-check)
	mkdir -p $${HOME}/.cache/dependency-check
	docker run --pull always --rm \
			--workdir="/project" \
			-e user=$$USER \
			-u $(shell id -u $${USER}):$(shell id -g $${USER}) \
			--volume $${HOME}/.cache/dependency-check:/usr/share/dependency-check/data:z \
			--volume $(PROJECT_DIR):/project:rw \
			--volume $(PROJECT_DIR)/reports:/reports:rw \
			owasp/dependency-check:latest \
			--scan /project \
			$(DEPENDENCY_CHECK_OUTPUT_ARG) \
			--prettyPrint \
			--enableExperimental \
			--project "dependency-check scan: $(PROJECT_NAME)"

# Alternate output : as html file
dependency-check-html: dependency-check
dependency-check-html: DEPENDENCY_CHECK_OUTPUT_ARG := --format "HTML" --out /project/reports/sca-dependency-check-$(PROJECT_NAME).html
# Alternate output : as gitlab-ci report
dependency-check-gitlab: dependency-check
dependency-check-gitlab: DEPENDENCY_CHECK_OUTPUT_ARG := --format "JSON" --out /project/reports/gl-sca-dependency-check-$(PROJECT_NAME).json

# Snyk Open Source
# ------------------------------------------------------------------------------------------------
snyk-open-source: snyk-open-source-gradle snyk-open-source-python snyk-open-source-nodejs
snyk-open-source-gitlab: snyk-open-source-gradle-gitlab snyk-open-source-python-gitlab snyk-open-source-nodejs-gitlab

ifeq ($(IS_GRADLE),yes)
SNYK_OPEN_SOURCE_GRADLE_OUTPUT_ARG := --env SARIF_OUTPUT="--sarif-file-output=reports/sca-snyk-open-source-gradle.sarif"
snyk-open-source-gradle: init-reports
	$(warning ###################################################################)
	$(warning Running snyk open-source for gradle)
	docker run --pull always --rm \
			--env SNYK_TOKEN \
			$(SNYK_OPEN_SOURCE_GRADLE_OUTPUT_ARG) \
			-v $(PROJECT_DIR):/app \
			snyk/snyk:gradle \
		|| true
else 
# No need to run if this is not a gradle project :)
snyk-open-source-gradle:
endif

ifeq ($(IS_PYTHON),yes)
SNYK_OPEN_SOURCE_PYTHON_OUTPUT_ARG := --env SARIF_OUTPUT="--sarif-file-output=reports/sca-snyk-open-source-python.sarif"
snyk-open-source-python: init-reports
	$(warning ###################################################################)
	$(warning Running snyk open-source for python)
	docker run --pull always --rm \
			--env SNYK_TOKEN \
			$(SNYK_OPEN_SOURCE_OUTPUT_ARG) \
			-v $(PROJECT_DIR):/app \
			snyk/snyk:python \
		|| true
else 
# No need to run if this is not a python project :)
snyk-open-source-python:
endif

ifeq ($(IS_NODEJS),yes)
SNYK_OPEN_SOURCE_NODEJS_OUTPUT_ARG := --env SARIF_OUTPUT="--sarif-file-output=reports/sca-snyk-open-source-nodejs.sarif"
snyk-open-source-nodejs: init-reports
	$(warning ###################################################################)
	$(warning Running snyk open-source for nodejs)
	docker run --pull always --rm \
			--env SNYK_TOKEN \
			$(SNYK_OPEN_SOURCE_OUTPUT_ARG) \
			-v $(PROJECT_DIR):/app \
			snyk/snyk:node \
		|| true
else 
# No need to run if this is not a nodejs project :)
snyk-open-source-nodejs:
endif

# Snyk is not producing a gitlab-ci compatible output json file
snyk-open-source-gradle-gitlab: snyk-open-source-gradle
snyk-open-source-gradle-gitlab: SNYK_OPEN_SOURCE_OUTPUT_ARG := --env JSON_OUTPUT="--json-file-output=reports/sca-snyk-open-source-python.json"
# Snyk is not producing a gitlab-ci compatible output json file
snyk-open-source-python-gitlab: snyk-open-source-python
snyk-open-source-python-gitlab: SNYK_OPEN_SOURCE_OUTPUT_ARG := --env JSON_OUTPUT="--json-file-output=reports/sca-snyk-open-source-gradle.json"
# Snyk is not producing a gitlab-ci compatible output json file
snyk-open-source-nodejs-gitlab: snyk-open-source-nodejs
snyk-open-source-nodejs-gitlab: SNYK_OPEN_SOURCE_OUTPUT_ARG := --env JSON_OUTPUT="--json-file-output=reports/sca-snyk-open-source-nodejs.json"

##################################################################################################
# Container scanning
##################################################################################################
container-scanning: trivy-container-scanning docker-scan

# Trivy container scanning
# ------------------------------------------------------------------------------------------------
# Default output for trivy container scanning
TRIVY_CONTAINER_OUTPUT_ARG := --format template --template "@contrib/sarif.tpl" -o /project/reports/container-scanning-trivy-$(PROJECT_NAME).sarif

ifeq ($(IS_DOCKER),yes)
trivy-container-scanning: build init-reports
	$(warning ###################################################################)
	$(warning Container security testing with trivy)
	docker run --pull always --rm \
			-v $${HOME}/.cache/trivy:/root/.cache/trivy:rw \
			-v /var/lib/docker:/root/.cache/:rw \
			-v /var/run/docker.sock:/var/run/docker.sock:rw \
			-v $(PROJECT_DIR):/project/:rw \
			aquasec/trivy:latest \
			image --no-progress $(TRIVY_CONTAINER_OUTPUT_ARG) $(DOCKER_IMAGE_TAGNAME)-$(BUILD_ARCH)
else 
# No need to run if this is not a docker project :)
trivy-container-scanning:
endif

# Alternate output : as table on stdout
trivy-container-scanning-stdout: trivy-sast
trivy-container-scanning-stdout: TRIVY_CONTAINER_OUTPUT_ARG = --format table
# Alternate output : as gitlab-ci report
trivy-container-scanning-gitlab: trivy-container-scanning
trivy-container-scanning-gitlab: TRIVY_CONTAINER_OUTPUT_ARG = --format template --template "@contrib/gitlab.tpl" -o /project/reports/gl-container-scanning-trivy-$(PROJECT_NAME).json

# docker scan with snyk
# ------------------------------------------------------------------------------------------------
DOCKER_SCAN_OUTPUT_ARG := 

ifeq ($(IS_DOCKER),yes)
docker-scan: setup-docker-scan init-reports
	$(warning ###################################################################)
	$(warning Container security testing with docker scan / snyk)
	docker scan -f $(PROJECT_DIR)/Dockerfile --exclude-base \
			$(DOCKER_IMAGE_TAGNAME)-$(BUILD_ARCH) \
			$(DOCKER_SCAN_OUTPUT_ARG)
else 
# No need to run if this is not a docker project :)
docker-scan:
endif

# Snyk is not producing a gitlab-ci compatible output json file
docker-scan-gitlab: docker-scan
docker-scan-gitlab: DOCKER_SCAN_OUTPUT_ARG = --json > $(PROJECT_DIR)/reports/container-scanning-docker-scan-$(PROJECT_NAME).json

##################################################################################################
# Infra-as-code scanning
##################################################################################################
iac-sast: kics-iac-sast

# Checkmarx Kics
# ------------------------------------------------------------------------------------------------
# Default output for kics iac-sast
KICS_OUTPUT_ARG =	--report-formats sarif --output-name sast-iac-kics-$(PROJECT_NAME)

kics-iac-sast: init-reports
	$(warning ###################################################################)
	$(warning IaC SAST with KICS)
	docker run --pull always --rm \
			--workdir="/project" \
			--entrypoint "" \
			-v $(PROJECT_DIR):/project/:rw \
			checkmarx/kics:latest \
			kics scan -q /app/bin/assets/queries \
			-p /project/ \
			--ignore-on-exit all \
			--log-level=DEBUG \
			--log-path /dev/stdout \
			$(KICS_OUTPUT_ARG) \
			-o /project/reports/

# Alternate output : as gitlab-ci report
kics-iac-sast-gitlab: kics-iac-sast
kics-iac-sast-gitlab: KICS_OUTPUT_ARG =	--report-formats glsast --output-name gl-sast-iac-kics-$(PROJECT_NAME)

##################################################################################################
# Release
##################################################################################################
release: test setup-qemu
	$(warning ###################################################################)
	$(warning Cross-compiling multi-arch image and pushing to registry)
	docker buildx build --pull --push \
			--cache-from type=registry,ref=$(CI_REGISTRY_IMAGE):cache \
			--platform $(RELEASE_ARCH) \
			--cache-to type=registry,ref=$(CI_REGISTRY_IMAGE):cache  \
			--label "org.opencontainers.image.authors=$(PROJECT_MAINTAINER)" \
			--label "org.opencontainers.image.created=$(BUILD_DATE)" \
			--label "org.opencontainers.image.base.name=$(BASE_IMAGE)" \
			--label "org.opencontainers.image.version=$(CI_COMMIT_REF_NAME)" \
			--label "org.opencontainers.image.revision=$(CI_COMMIT_SHA)" \
			--label "org.opencontainers.image.source=https://gitlab.com/$(PROJECT_GROUP)/$(PROJECT_NAME)" \
			--label "org.opencontainers.image.licenses=$(LICENCES)" \
			--label "built-for=$(RELEASE_ARCH)" \
			-t $(DOCKER_IMAGE_TAGNAME) \
			-t $(DOCKER_IMAGE_TAGMAJORMINOR) \
			-t $(DOCKER_IMAGE_TAGLATEST) .

##################################################################################################
# Utility
##################################################################################################
docker-login:
	docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" ${CI_REGISTRY}

docker-save-image:
	mkdir -p build/
	docker save $(DOCKER_IMAGE_TAGNAME)-$(BUILD_ARCH) > build/$(PROJECT_NAME)-image.tar

docker-load-image:
	docker load -i build/*-image.tar
	rm build/*-image.tar

install-buildx:
	$(warning ###################################################################)
	$(warning Installing buildx)
	docker buildx version 2>/dev/null || ( \
			mkdir -p ~/.docker/cli-plugins \
			&& wget -O ~/.docker/cli-plugins/docker-buildx $(shell curl https://api.github.com/repos/docker/buildx/releases/latest 2>/dev/null | grep "browser_download_url.*$(BUILD_ARCH)" | awk '{print $$2}' | tr -d '"') \
    	&& chmod a+x ~/.docker/cli-plugins/docker-buildx )
	docker buildx ls | grep docker-container || docker buildx create --use

setup-qemu: install-buildx
	$(warning ###################################################################)
	$(warning Setting up qemu)
	docker buildx use $(shell docker buildx ls | grep docker-container | head -1 | awk '{print $$1}')
	docker run --rm --privileged multiarch/qemu-user-static --reset -p yes

# For NodeJS projects we have to extract the dependencies from the builder stage of the Dockerfile
ifeq ($(IS_NODEJS),yes)
update-dependencies-from-builder: replace-node-modules
else
update-dependencies-from-builder: 
endif

replace-node-modules: compile
	$(warning ###################################################################)
	$(warning Replacing nodejs modules in project home from builder)
	docker container rm -f $(PROJECT_NAME)-extract 2>/dev/null
	docker container create --name $(PROJECT_NAME)-extract $(DOCKER_IMAGE_TAGNAME)-$(BUILD_ARCH)-builder
	rm -rf $(PROJECT_DIR)/node_modules
	docker container cp $(PROJECT_NAME)-extract:/ng-app/node_modules $(PROJECT_DIR)/
	docker container rm -f $(PROJECT_NAME)-extract

setup-docker-scan:
	$(warning ###################################################################)
	$(warning Setting up docker-scan)
	docker scan --accept-license --version || ( \
		mkdir -p ~/.docker/cli-plugins && \
		curl https://github.com/docker/scan-cli-plugin/releases/latest/download/docker-scan_linux_amd64 -L -s -S -o ~/.docker/cli-plugins/docker-scan &&\
		chmod +x ~/.docker/cli-plugins/docker-scan )
	docker scan --login --token ${SNYK_TOKEN} --accept-license

init-reports:
	mkdir -p $(PROJECT_DIR)/reports

snyk-login:
	docker run --pull always --rm \
			-v ~/.config/configstore/snyk.json:/root/.config/configstore/snyk.json:rw \
			-v $(PROJECT_DIR)/reports:/output/:rw \
			-v $(PROJECT_DIR):/project/:rw \
			-e SNYK_TOKEN \
			--entrypoint "" \
			snyk/snyk-cli:docker \
			sh -c 'snyk auth $${SNYK_TOKEN}'
